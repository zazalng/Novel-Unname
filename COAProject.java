package coaproject;

import java.util.Scanner;
import java.util.Random;
import java.lang.Object;

public class COAProject {
    
    public static String [][] memoryData; // 0 = Memory Address | 1 = Register Address
    public static String [] memoryRegisterType = {"R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8", "R9", "R10", "R11", "BX", "BP", "SI", "DI"};
    
    public static int assemblyLOAD(String address, String operand, boolean isAddress){
        int valueFlag = 0;
        if(isAddress){
            
        } else{
            valueFlag = 1;
        }
        return valueFlag;
    }
    
    public static int assemblyMOV(String address, String operand, boolean isAddress){
        int valueFlag = 0;
        
        return valueFlag;
    }
    
    public static int assemblyADD(String objectY, String objectX, boolean isAddress){
        int valueFlag = 0;
        
        return valueFlag;
    }
    
    public static int assemblySUB(String objectY, String objectX, boolean isAddress){
        int valueFlag = 0;
        
        return valueFlag;
    }
    
    public static int assemblyMUL(String objectY, String objectX, boolean isAddress){
        int valueFlag = 0;
        
        return valueFlag;
    }
    
    public static int assemblySTORE(String address, String operand, boolean isAddress){
        int valueFlag = 0;
        
        return valueFlag;
    }
    
    public static void programHelpArgs(String command){
        String [] commandList = {"convert", "set" };
        
        if(command.equalsIgnoreCase(commandList[0])){
            System.out.printf("help %s | [Args] = {register, ram, null (show both Ram (H) & Register (H) memory)}", command);
        } else if(command.equalsIgnoreCase(commandList[1])){
            System.out.printf("help %s | Exit Program", command);
        }
    }
    
    public static void programMemoryVisual(int memorySize){
        System.out.printf("Memory Data\t\t\tRegister Data\n");
        for(int i = memorySize - 1; i >= 0; i--){
            String nameAddress = Integer.toHexString(i);
            try{
                System.out.printf("%sH = %s\t\t\t\t", nameAddress.toUpperCase(), memoryData[0][i]);
            
                System.out.printf("%s = %s\n", memoryRegisterType[i], memoryData[1][i]);
            }catch(ArrayIndexOutOfBoundsException e){
                System.out.printf("\n");
                continue;
            }
        }
    }
    
    public static boolean isNumberic(String stringChecker){
        try{
            int intCheck = Integer.parseInt(stringChecker);
        } catch(NumberFormatException nfe){
            return false;
        }
            return true;
    }
    
    public static String heximalToDecimal(String heximalText){
        int decimalNumber = Integer.parseInt("heximalText", 16);
        String returnText = Integer.toString(decimalNumber);
        return returnText;
    }
    
    public static void main(String[] args) {
        Scanner scanf = new Scanner(System.in);
        Random rand = new Random();
        
        int memorySize;
        int typeFlag = 0;
        String commandString = "";
        String [] commandList = {"HELP", "MEMORY", "EXIT", "LOAD", "MOV", "ADD", "SUB", "MUL", "STORE", "SET"};
        String [] memoryTypeFlag = {"", "Immediate Addressing Mode", "Direct Addressing Mode", "Indirect Addressing Mode", "Resgister Direct Addressing Mode", "Register Indirect Addressing Mode", "Indexed Addressing Mode", "Relative Addressing Mode", "Stack Addressing Mode"};
        
        while(true){
            System.out.printf("Initialize Memory Size\nPlease enter your size of your memory (mininum 16): ");
            commandString = scanf.nextLine();
            boolean logicCheck = isNumberic(commandString);
            if(logicCheck){
                memorySize = Integer.parseInt(commandString);
                if(memorySize > 15){
                    break;
                } else{
                    System.out.printf("Memory size is lower than 16, Force to set memory size as 16.\n");
                    memorySize = 16;
                    break;
                }
            } else{
                
            }
        }
        
        { // Initialize Memory
            memoryData = new String [2][memorySize];
            
            while(true){
                System.out.printf("Memory size has been set at %d\nDo you want to input Data by yourselves or not (Y/N):", memorySize);
                commandString = scanf.next();

                if(commandString.equalsIgnoreCase("y")){
                    for(int i = 0; i < memorySize; i++){
                        commandString = Integer.toHexString(i);
                        System.out.printf("Address %sH = ", commandString.toUpperCase());
                        commandString = scanf.next();
                        boolean logicChecker = isNumberic(commandString);
                        if(logicChecker){
                            memoryData[0][i] = commandString;
                        } else{
                            System.out.printf("Wrong Input, Try Again\n\n");
                            i--;
                        }
                    }
                    break;
                } else if(commandString.equalsIgnoreCase("n")){
                    for(int i = 0; i < memorySize; i++){
                        int randNumber = rand.nextInt(100 + 1 - 1) + 1;
                        commandString = Integer.toHexString(i);
                        System.out.printf("Address %sH = %d\n", commandString.toUpperCase(), randNumber);
                        memoryData[0][i] = Integer.toString(randNumber);
                    }
                    break;
                } else{
                    System.out.printf("Wrong Input, try again\n\n");
                    continue;
                }
            }
        }
        
        System.out.printf("Finishing Initialize\n\nWelcome to Assembly Program simulate\nThis program allow user to play with Heximal and Decimal only for now\nWhen instuction has been input, progaram will automatic convert Heximal to Decimal and print out result in Decimal format\nFor new user please type 'help' to learn how this program work\n");
            
        
        
        scanf.nextLine();
        while(true){
            commandString = scanf.nextLine();
            commandString = commandString.toLowerCase();
                if(commandString.equalsIgnoreCase("help")){
                    System.out.printf("---Command for Program---\nhelp [command]\t\t| Show this instruction / with [command] you can know [args] on other command"
                            + "\nmemory\t\t\t| Show memory data table visual"
                            + "\nset [args1] [args2]\t| Set [args1] address to value same as [args2]"
                            + "\nexit\t\t\t| Exit Program\n\n");
                    System.out.printf("---Assembly Instruction---"
                            + "\nLOAD [operand1] [operand2]\t| Load data from [operand2] into [operand1]. Only work for [operand1] as register and [operand2] as memory or value"
                            + "\nMOV [operand1] [operand2]\t| Move data from [operand2] into [operand1]. Only work for [operand1] as register and [operand2] as register or value"
                            + "\nSTORE [operand1] [operand2]\t| Store data from [operand2] into [operand1]. Only work for [operand1] as memory and [operand2] as register or value"
                            + "\nADD [operand1] [operand2]\t| [operand1] + [operand2] If [operand1] and [operand2] is same base number value will plus together"
                            + "\nSUB [operand1] [operand2]\t| [operand1] - [operand2] If [operand1] and [operand2] is same base number value will subtract together"
                            + "\nMUL [operand1] [operand2]\t| [operand1] * [operand2] If [operand1] and [operand2] is same base number value will mulitple together");
                    System.out.printf("\n");
                } else{
                    //commandList = {"HELP", "MEMORY", "EXIT", "LOAD", "MOV", "ADD", "SUB", "MUL", "STORE", "SET"}
                    try{
                        String [] commandSplit = commandString.split(" "); // [1] = operand1 [2] = operand2
                        commandString = commandSplit[0];
                        if(commandString.equalsIgnoreCase(commandList[0])){ // Command: Help
                            programHelpArgs(commandSplit[1]);
                        } else if(commandString.equalsIgnoreCase(commandList[1])){ //Command: Memory (Memory Viewer)
                            programMemoryVisual(memorySize);
                        } else if(commandString.equalsIgnoreCase(commandList[2])){ //Command: Exit
                            System.out.printf("Exiting Program\n");
                            break;
                        } else if(commandString.equalsIgnoreCase(commandList[3])){ //Command: LOAD
                            if(commandSplit[1].startsWith("r")){ // Know as Register
                                if(commandSplit[2].startsWith("#")){ //
                                    typeFlag = assemblyLOAD(commandSplit[1], commandSplit[2].replace("#", ""), false);
                                } else if(commandSplit[2].endsWith("h")){
                                    typeFlag = assemblyLOAD(commandSplit[1], commandSplit[2], true);
                                } else if((commandSplit[2].startsWith("[")) && (commandSplit[2].endsWith("]"))){
                                    typeFlag = assemblyLOAD(commandSplit[1], commandSplit[2], true);
                                } else if(commandSplit[2].equalsIgnoreCase("stack")){
                                    
                                } else{
                                    System.out.printf(commandSplit[1]);
                                    System.out.printf("Wrong Input, Tyr Again.\nLOAD [operand1] [operand2]\t| Load data from [operand2] into [operand1]. Only work for [operand1] as register and [operand2] as memory or value\n");
                                }
                            } else{
                                System.out.printf(commandSplit[1]);
                                System.out.printf("Wrong Input, Tyr Again.\nLOAD [operand1] [operand2]\t| Load data from [operand2] into [operand1]. Only work for [operand1] as register and [operand2] as memory or value\n");
                            }
                        } else if(commandString.equalsIgnoreCase(commandList[4])){ //Command: MOV
                            
                        } else if(commandString.equalsIgnoreCase(commandList[5])){ //Command: ADD

                        } else if(commandString.equalsIgnoreCase(commandList[6])){ //Command: SUB

                        } else if(commandString.equalsIgnoreCase(commandList[7])){ //Command: MUL

                        } else if(commandString.equalsIgnoreCase(commandList[8])){ //Command: STORE

                        } else if(commandString.equalsIgnoreCase(commandList[9])){ //Command: Set (Set address to value)
                            
                        } else{
                            System.out.printf("Wrong Input, Tyr Again.");
                        }
                    } catch(ArrayIndexOutOfBoundsException e){
                        continue;
                    }
                    
                    System.out.printf("Using: %s", memoryTypeFlag[typeFlag]);
                }                
        }
    } 
}