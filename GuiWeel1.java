package guiweel1;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.util.Random;

public class GuiWeel1 {
    public static void main(String[] args) {
        /*Frame frameMain = new Frame("Multiple Button");
        frameMain.setSize(500, 500);
        
        Panel panelMain = new Panel();
        panelMain.setBounds(0,30,500,450);
        
        frameMain.setLayout(null);
        panelMain.setLayout(new GridLayout(5, 4));
        
        Button btnNo1 = new Button("Button 1");
        Button btnNo2 = new Button("Button 2");
        Button btnNo3 = new Button("Button 3");
        Button btnNo4 = new Button("Button 4");
        Button btnNo5 = new Button("Button 5");
        Button btnNo6 = new Button("Button 6");
        Button btnNo7 = new Button("Button 7");
        Button btnNo8 = new Button("Button 8");
        Button btnNo9 = new Button("Button 9");
        Button btnNo10 = new Button("Button 10");
        Button btnNo11 = new Button("Button 11");
        Button btnNo12 = new Button("Button 12");
        Button btnNo13 = new Button("Button 13");
        Button btnNo14 = new Button("Button 14");
        Button btnNo15 = new Button("Button 15");
        Button btnNo16 = new Button("Button 16");
        Button btnNo17 = new Button("Button 17");
        Button btnNo18 = new Button("Button 18");
        Button btnNo19 = new Button("Button 19");
        Button btnNo20 = new Button("Button 20");
        
        panelMain.add(btnNo1);
        panelMain.add(btnNo2);
        panelMain.add(btnNo3);
        panelMain.add(btnNo4);
        panelMain.add(btnNo5);
        panelMain.add(btnNo6);
        panelMain.add(btnNo7);
        panelMain.add(btnNo8);
        panelMain.add(btnNo9);
        panelMain.add(btnNo10);
        panelMain.add(btnNo11);
        panelMain.add(btnNo12);
        panelMain.add(btnNo13);
        panelMain.add(btnNo14);
        panelMain.add(btnNo15);
        panelMain.add(btnNo16);
        panelMain.add(btnNo17);
        panelMain.add(btnNo18);
        panelMain.add(btnNo19);
        panelMain.add(btnNo20);
        
        frameMain.add(panelMain);
        
        frameMain.setVisible(true);*/
        /*Button btn;
        for(int i = 0; i < 20; i++){
            btn = new Button("btn"+i);
            System.out.println(btn.toString());
        }*/
        
        GuiWeel1 self = new GuiWeel1();
        self.callFrameNo1();
        self.callFrameNo2();
        self.callFrameNo3();
        self.callFrameNo4();
    }
    
    /*public String scanf(){
        //new 
    }*/
    
    public void callFrameNo1(){
        Frame frameMain = new Frame("Multiple Button");
        frameMain.setSize(500, 500);
        
        Panel panelMain = new Panel();
        panelMain.setBounds(0,30,500,450);
        
        frameMain.setLayout(null);
        panelMain.setLayout(new GridLayout(5, 4));
        
        Button btn;
        for(int i = 0; i < 20; i++){
            btn = new Button("Button " + i);
            panelMain.add(btn);
        }
        
        frameMain.add(panelMain);
        
        frameMain.setVisible(true);
    }
    public void callFrameNo2(){
        Frame frameMain = new Frame("Multiple Button");
        frameMain.setSize(500, 500);
        
        Panel panelMain = new Panel();
        panelMain.setBounds(0,30,500,450);
        
        frameMain.setLayout(null);
        panelMain.setLayout(new GridLayout(8, 5));
        
        Button btn;
        TextArea txtArea;
        for(int i = 0; i < 20; i++){
            btn = new Button("Button " + i);
            panelMain.add(btn);
            txtArea = new TextArea("",10,10,TextArea.SCROLLBARS_NONE);
            panelMain.add(txtArea);
        }
        
        frameMain.add(panelMain);
        
        frameMain.setVisible(true);
    }
    public void callFrameNo3(){
        int width = 500;
        int height = 500;
        int windowBorder = 30;
        Random rnd = new Random();
        Frame frameMain = new Frame("Multiple Button");
        frameMain.setSize(width, height);
        
        Panel panelMain = new Panel();
        panelMain.setBounds(0,windowBorder,width,height);
        
        frameMain.setLayout(null);
        panelMain.setLayout(null);
        
        Button btn;
        for(int i = 0; i < 50; i++){
            btn = new Button(""+i);
            btn.setBounds(rnd.nextInt(width - 0) + 1 - 0, rnd.nextInt(width - 0) +1 - 0, 50, 50);
            btn.setBackground(new Color(rnd.nextInt(16777215 - 0)+1 - 0));
            panelMain.add(btn);
        }
        
        frameMain.add(panelMain);
        
        frameMain.setVisible(true);
    }
    
    public void callFrameNo4(){
        int width = 500;
        int height = 500;
        int windowBorder = 30;
        Frame frameMain = new Frame("Multiple Button");
        frameMain.setSize(width, height);
        frameMain.setLayout(null);
        
        Panel panelMain = new Panel();
        panelMain.setLayout(new BorderLayout());
        panelMain.setBounds(0 + 50, windowBorder, frameMain.getWidth() - 100, frameMain.getHeight() - 150);
        
        TextArea txtArea = new TextArea("",10,10,TextArea.SCROLLBARS_NONE);
        panelMain.add(txtArea, BorderLayout.NORTH);
        
        
        frameMain.add(panelMain);
        frameMain.setVisible(true);
    }
}
