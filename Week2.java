package week2;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author ITMSU
 */
public class Week2 {

    public static void main(String[] args) {
        Scanner scanf = new Scanner(System.in);
        
        {
            System.out.println("*****Problem 2*****");
            int x, y;
     
            System.out.print("Enter Num X: ");
            x = scanf.nextInt();
            System.out.print("Enter Num Y: ");
            y = scanf.nextInt();

            System.out.println(x + " + " + y + " = " + (x+y));
            System.out.println(x + " - " + y + " = " + (x-y));
            System.out.println(x + " * " + y + " = " + (x*y));
            System.out.printf("%d + %d = %.2f", x, y, (float)(x / y))
            System.out.println(x + " % " + y + " = " + (x%y));
        }
        
        {
            System.out.println("*****Problem 3*****");
            int price, amt, getMoney;
            String nameProduct;
            
            System.out.print("Enter Product name: ");
            scanf.nextLine();
            nameProduct = scanf.nextLine();
            System.out.print("Enter Price: ");
            price = scanf.nextInt();
            System.out.print("Enter Amount: ");
            amt = scanf.nextInt();
            System.out.print("Received: ");
            getMoney = scanf.nextInt();
            
            System.out.println("********************");
            System.out.println("Product Name: " + nameProduct + " | Price per unit: " + price + " | Amount: " + amt);
            System.out.println("Cost in total: " + (price * amt));
            System.out.println("Change: " + (getMoney - (price * amt)));
        }
        
        {
            System.out.println("*****Problem 4*****");
            int incomeDad;
            System.out.print("Income: ");
            incomeDad = scanf.nextInt();
            
            System.out.println("*******************");
            System.out.println("Mom have: " + (float)45 / 100 * incomeDad + " (45%)");
            System.out.println("Child have: " + (float)15 / 100 * incomeDad + " (15%)");
            System.out.println("Dad have Remain: " + (float)40 / 100 * incomeDad);
        }
        
        {
            System.out.println("*****Problem 5*****");
            System.out.print("Enter de full-Price: ");
            int price = scanf.nextInt();
            
            System.out.println("*******************");
            System.out.println("Discount: " + 30.0/100*price);
            System.out.println("New Price: " + ((price*1.0) - (30.0/100*price)));
        }
        
        {
            System.out.println("*****Problem 6*****");
            System.out.print("Enter de rice (kg): ");
            int rice = scanf.nextInt();
            
            System.out.println("*******************");
            System.out.println("Package (12kg / unit): " + (rice / 12));
            System.out.println("Remaining: " + (rice % 12));
        }
        
        {
            System.out.println("*****Problem 7*****");
            System.out.print("Mango amount (kg): ");
            int mangoAmt = scanf.nextInt();
            
            System.out.println("*******************");
            System.out.println("Rambutan: " + (100.0/27*mangoAmt) + " (kg)");
            System.out.println("Longan: " + (85.0/100*mangoAmt) + " (kg)");
            System.out.println("Total Sum: " + ((85.0/100*mangoAmt)+(100.0/27*mangoAmt)+(1.0*mangoAmt)) + " (kg)");
        }
    }
    
}
