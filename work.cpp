#include <stdio.h>
#include <iostream>
#include <cstring>
#include <string>
#include <math.h>
#include <conio.h>
void demo01() {
	//create by : Napapon Kamanee
	// date: 21/8/61
	/* create by Napapon Kamanee
	   date 21/08/61
	   program name : input number
	*/
	int num;
	printf("\nEnter number:> ");
	scanf("%d", &num);
	printf("you input number: %d", num);
	getch();
}

void demo02() {
	int num;
	float data;
	num = 10;
	data = 10.5;
	printf("\nHello\n");
	printf("num = %d\n", num);
	printf("data = %f\n", data);
	printf("%d, %f\n", num, data);
	printf("%d, %f, %d, %c\n", num, data, 100, 'x');
	getch();
}

void demo03() {
	int age = 30;
	float num = 10.5;
	char sec = '1';
	char *name = "msu";
	printf("\nhello ");
	printf("world \n");
	printf("age is %d \n", age);
	printf("number is %.2f \n", num);
	printf("sec is %c \n", sec);
	printf("name is %s \n", name);
	printf("age=%d, number is %.2f \n", age, num);
	printf("sec is %c, name is %s ", sec, name);
	printf("%d, %.4f\n", 100, 0.75);
	getch();
}

void demo04() {
	//var input
	int number1, number2;
	//var output
	int result;

	printf("input number1 :> ");
	scanf("%d", &number1);
	printf("input number2 :> ");
	scanf("%d", &number2);

	result = number1 + number2;

	printf("result is %d\n", result);
	printf("%d + %d = %d\n", number1, number2, result);
	getch();
}

void Ex01() {
	float width, lenght;

	float area;

	printf("\nPlease enter lenght:");
	scanf("%f", &lenght);
	printf("\nPlease enter width:");
	scanf("%f", &width);

	area = (lenght*width) / 2;

	printf("\nArea is %.2f\n", area);
	getch();
}

void Ex02() {
	int number1, number2, number3;

	float result;

	printf("\nPlease enter number1:");
	scanf("%d", &number1);
	printf("\nPlease enter number2:");
	scanf("%d", &number2);
	printf("\nPlease enter number3:");
	scanf("%d", &number3);

	result = (number1 + number2 + number3) / 3.00;

	printf("Result is %.2f\n", result);
	getch();
}

void Ex03() {
	int radius;

	int cycle;
	float area;

	printf("\nEnter radius:");
	scanf("%i", &radius);
	area = 3.14*(radius*radius);
	cycle = (2 * 3.14 * radius);
	printf("Area is %f\n", area);
	printf("Cycle is %d\n", cycle);
	getch();
}

void Week3() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Week 3 (21/08/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu):");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\n1. demo01.cpp\n2. demo02.cpp\n3. demo03.cpp\n4. demo04.cpp\n");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu):");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					demo01();
					x = 0;
				}
				else if (x == 2) {
					demo02();
					x = 0;
				}
				else if (x == 3) {
					demo03();
					x = 0;
				}
				else if (x == 4) {
					demo04();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. Ex01\n2. Ex02\n3. Ex03\n");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu):");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					Ex01();
					x = 0;
				}
				else if (x == 2) {
					Ex02();
					x = 0;
				}
				else if (x == 3) {
					Ex03();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void demo0401() {
	int ans, x, y;
	printf("Enter x:> ");
	scanf("%d", &x);
	printf("Enter y:> ");
	scanf("%d", &y);
	printf("x = %d, y = %d\n", x, y);
	ans = x + y; printf("x+y = %d\n", ans);
	ans = x - y; printf("x-y = %d\n", ans);
	ans = x * y; printf("x*y = %d\n", ans);
	ans = x / y; printf("x/y = %d\n", ans);
	ans = x % y; printf("x%%y = %d\n", ans);
	float a, b = 10.0, c = 3.0;
	printf("b = %f, c= %f\n", b, c);
	a = b / c; printf("b / c = %f", a);
	getch();
}

void demo0402() {
	int a = 10, b = 20;
	float z, x = 10, y = 20;

	z = a / b; printf("z = %.2f\n", z);
	z = a / y; printf("z = %.2f\n", z);
	z = x / y; printf("z = %.2f\n", z);

	getch();
}

void Week4() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Week 4 (28/08/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu):");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\n1. demo0401.cpp\n2. demo0402.cpp\n");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu):");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					demo0401();
					x = 0;
				}
				else if (x == 2) {
					demo0402();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. No Excercise today");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu):");
				scanf("%d", &x);
				if (x == 0) {
					break;
				} /*else if(x==1){
					Ex0401();
					x = 0;
				}*/ else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void demo0501() {
	int dadSalary, momShare, childShare = 0;
	printf("Enter dad's salary: ");
	scanf("%d", &dadSalary);

	momShare = (dadSalary * 30) / 100;
	childShare = (dadSalary - momShare) / 3;

	printf("Mom will get %d\nTheir child will get %d per child", momShare, childShare);
}

void Ex0501() {
	int powRoot, num;
	float result = 0;
	printf("\nEnter number for setting power and root: ");
	scanf("%d", &powRoot);
	printf("\nEnter number for calculate: ");
	scanf("%d", &num);

	if (powRoot == 0) {
		printf("%d power by %d is 1\n", num, powRoot);
		printf("%d root by %d is undefined\n", num, powRoot);
	}
	else {
		result = pow(num, powRoot);
		printf("%d power by %d is %.0f\n", num, powRoot, result);
		result = pow(powRoot, -1);
		result = pow(num, result);
		printf("%d root by %d is %.2f\n", num, powRoot, result);
	}
	getch();
}

void Ex0502() {
	int speed, pointA, pointB, result = 0;
	float speedInMin = 0;
	printf("\nEnter how fast you are (km/h): ");
	scanf("%d", &speed);
	speedInMin = (speed / 60.00);
	printf("\nEnter first position (km): ");
	scanf("%d", &pointA);
	printf("\nEnter destination position (km): ");
	scanf("%d", &pointB);
	if (pointA == pointB) {
		printf("\nYou are stay at your destination\nCongratulations!!!");
	}
	else if (pointA > pointB) {
		result = pointA - pointB;
		speedInMin = (result / speedInMin);
		printf("\nYou are faraway from destination about %d km\nwith your current speed %d km/h\nYou will use about %.0f minute to arrive there\n", result, speed, speedInMin);
	}
	else {
		result = pointB - pointA;
		speedInMin = (result / speedInMin);
		printf("\nYou are faraway from destination about %d km\nwith your current speed %d km/h\nYou will use about %.0f minute to arrive there\n", result, speed, speedInMin);
	}
	getch();
}

void Week5() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Week 5 (04/09/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu):");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\n1. Demo01.cpp");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu):");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					demo0501();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. Ex01(Root/Power)\n2. Ex02 (Find distant and when will arrive)");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu):");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					//Ex0501();
					x = 0;
				}
				else if (x == 2) {
					//Ex0502();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void demo0601() {
	float x, y, result;
	bool p = true, q = true, r = false, ans;
	result = pow(2, pow(3, 2));
	printf("Result %.2f\n", result);
	result = pow(-1, 2);
	printf("Result %.f\n", result);
	ans = !(p || q) || 5 > -3 && 5 != 3;
	printf("ans %d\n", ans);
	getch();
}

void demo0602() {
	int money;
	int b1000, b500, b100;
	printf("Input money: ");
	scanf("%d", &money);
	b1000 = money / 1000;
	b500 = (money % 1000) / 500;
	b100 = ((money % 1000) % 500) / 100;
	printf("Exchange\nNote 1000 = %d\nNote 500 = %d\nNote 100 = %d", b1000, b500, b100);
	getch();
}

void demo0603() {
	int gainMoney = 0.00;
	int t1Team, t2Team = 0;

	float t1Share, t2Share, clubShare, t1TeamShare, t2TeamShare = 0.00;

	printf("Input bonus money (1:1M): ");
	scanf("%d", &gainMoney);
	printf("How many T1 team?: ");
	scanf("%d", &t1Team);
	printf("How many T2 team?: ");
	scanf("%d", &t2Team);

	t1Share = (gainMoney * 0.50);
	t2Share = (gainMoney * 0.30);
	clubShare = gainMoney - (t1Share + t2Share);

	t1TeamShare = t1Share / t1Team;
	t2TeamShare = t2Share / t2Team;

	printf("T1 get money: %.2fM\nT1 each team get money: %.2fM\n", t1Share, t1TeamShare);
	printf("T2 get money: %.2fM\nT2 each team get money: %.2fM\n", t2Share, t2TeamShare);
	printf("Club get money: %.2fM", clubShare);
	getch();
}

void demo0604() {
	int x, y;

	printf("Enter number for check odd or even: ");
	scanf("%d", &x);
	y = x % 2;
	if (y == 0) {
		printf("it is even");
	}
	else {
		printf("it is odd");
	}
	getch();
}

void demo0605() {
	float a, b, result;

	printf("Enter number for divined: ");
	scanf("%f", &a);
	printf("Enter number for divine by: ");
	scanf("%f", &b);
	if (b != 0.0) {
		result = a / b;
		printf("Result of %.f / %.f is %.f", a, b, result);
	}
	else {
		printf("Cannot divine by Zero (0)");
	}
	getch();
}

void Ex0601() {
	int x1, y1, x2, y2;

	float result;

	printf("Input coordinate of x1: ");
	scanf("%d", &x1);
	printf("Input coordinate of y1: ");
	scanf("%d", &y1);
	printf("Input coordinate of x2: ");
	scanf("%d", &x2);
	printf("Input coordinate of y2: ");
	scanf("%d", &y2);

	result = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
	printf("Distant of (%d,%d) to (%d, %d) is %.2f", x1, y1, x2, y2, result);
	getch();
}

void Ex0602() {
	int a, b;

	float result;

	printf("Input width: ");
	scanf("%d", &a);
	printf("Input height: ");
	scanf("%d", &b);

	result = sqrt(pow(a, 2) + pow(b, 2));
	printf("Pythagoras of %d and %d is %.2f", a, b, result);
	getch();
}

void Ex0603() {
	int a, b;

	int result;

	printf("Input number a: ");
	scanf("%d", &a);
	printf("Input number b: ");
	scanf("%d", &b);

	if (b > a) {
		result = a - b;
		printf("Result is %d", result);
	}
	getch();
}

void Ex0604() {
	int x;
	printf("Input number: ");
	scanf("%d", &x);

	if (x > 0) {
		printf("Positive Number");
	}
	getch();
}

void Ex0605() {
	int x, result1, result2;
	printf("Enter number: ");
	scanf("%d", &x);
	result1 = x % 3;
	result2 = x % 5;
	if (result1 == 0) {
		printf("3\n");
	}
	if (result2 == 0) {
		printf("5\n");
	}
	if ((result1 == 0) && (result2 == 0)) {
		printf("3 & 5\n");
	}
	getch();
}

void Week6() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Week 6 (11/09/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\n1. Demo01.cpp\n2. Demo02.cpp\n3. Demo03.cpp\n4. Demo04.cpp");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					demo0601();
					x = 0;
				}
				else if (x == 2) {
					demo0602();
					x = 0;
				}
				else if (x == 3) {
					demo0603();
					x = 0;
				}
				else if (x == 4) {
					demo0604();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. Ex01\n2. Ex02\n3. Ex03\n4. Ex04\n5. Ex05");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					Ex0601();
					x = 0;
				}
				else if (x == 2) {
					Ex0602();
					x = 0;
				}
				else if (x == 3) {
					Ex0603();
					x = 0;
				}
				else if (x == 4) {
					Ex0604();
					x = 0;
				}
				else if (x == 5) {
					Ex0605();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void demo0701() {
	int a, b, result;

	printf("Read a: ");
	scanf("%d", &a);
	printf("Read b: ");
	scanf("%d", &b);

	if (a > b) {
		result = a - b;
	}
	else {
		result = a + b;
	}

	printf("Result is %d", result);

	getch();
}

void demo0702() {
	int a, b;

	printf("Read a: ");
	scanf("%d", &a);
	printf("Read b: ");
	scanf("%d", &b);

	if (a > b) {
		printf("%d is highest number", a);
		printf("%d is lowest number", b);
	}
	else if (a < b) {
		printf("%d is highest number", b);
		printf("%d is lowest number", a);
	}
	else {
		printf("They are equal to each other");
	}

	getch();
}

void demo0703() {
	int score;
	char *name = "";
	char *str = "";

	printf("Input score: ");
	scanf("%d", &score);
	printf("Enter Name: ");
	scanf("%s", &name);

	if (score >= 50) {
		str = "pass";
	}
	else {
		str = "fail";
	}

	printf("%s has %d score, You are %s", name, score, str);
}

void Ex0701() {
	int a, result;

	printf("Read a: ");
	scanf("%d", &a);

	if (a >= 0) {
		result = a;
	}
	else {
		result = a * (-1);
	}

	printf("Absolute of %d is %d", a, result);

	getch();
}

void Week7() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Week 7 (18/09/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\n1. Demo01.cpp\n2. Demo02.cpp\n3. Demo03.cpp");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					demo0701();
					x = 0;
				}
				else if (x == 2) {
					demo0702();
					x = 0;
				}
				else if (x == 3) {
					demo0703();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. Ex01");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					Ex0701();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void demo0801() {
	int x;
	printf("Enter Number: ");
	scanf("%d", &x);

	if (x > 0) {
		printf("Positive\n");
	}
	else if (x < 0) {
		printf("Negative\n");
	}
	else {
		printf("Zero\n");
	}

	getch();
}

void Ex0801() {
	int grade;
	char *strGrade = "";
	printf("Enter score: ");
	scanf("%d", &grade);

	if (grade >= 80) {
		strGrade = "A";
	}
	else if (grade >= 75) {
		strGrade = "B+";
	}
	else if (grade >= 70) {
		strGrade = "B";
	}
	else if (grade >= 65) {
		strGrade = "C+";
	}
	else if (grade >= 60) {
		strGrade = "C";
	}
	else if (grade >= 55) {
		strGrade = "D+";
	}
	else if (grade >= 50) {
		strGrade = "D";
	}
	else if (grade >= 0) {
		strGrade = "F";
	}
	else {
		printf("Wrong Input");
	}
	printf("%s", strGrade);
	getch();
}

void Ex0802() {
	int totalSales;
	float payBack;
	char *award = "";
	printf("Enter Total Sales value: ");
	scanf("%d", &totalSales);

	if (totalSales >= 1000000) {
		payBack = totalSales * (0.20 / 100);
		award = "Nissan March";
	}
	else if (totalSales >= 500000) {
		payBack = totalSales * (0.15 / 100);
		award = "Gold 2฿";
	}
	else if (totalSales >= 300000) {
		payBack = totalSales * (0.10 / 100);
		award = "Gold 1฿";
	}
	else if (totalSales >= 100000) {
		payBack = totalSales * (0.5 / 100);
		award = "-";
	}
	else if (totalSales < 100000) {
		payBack = 0;
		award = "-";
	}
	else {
		printf("Wrong Input");
	}
	printf("You have %.2f commission value\n", payBack);
	printf("Additional Award is %s", award);
	getch();
}

void Ex0803() {
	int num1, num2, num3;

	printf("Enter Number 1: ");
	scanf("%d", &num1);
	printf("Enter Number 2: ");
	scanf("%d", &num2);
	printf("Enter Number 3: ");
	scanf("%d", &num3);

	if (num1 > num2 && num1 > num3) {
		printf("%d is highest number", num1);
	}
	else if (num2 > num1 && num2 > num3) {
		printf("%d is highest number", num2);
	}
	else if (num3 > num1 && num3 > num3) {
		printf("%d is highest number", num3);
	}
	else if (num1 > num3 && num1 == num2) {
		printf("%d and %d is highest number", num1, num2);
	}
	else if (num1 > num2 && num1 == num3) {
		printf("%d and %d is highest number", num1, num3);
	}
	else if (num2 > num3 && num2 == num1) {
		printf("%d and %d is highest number", num2, num1);
	}
	else if (num2 > num1 && num2 == num3) {
		printf("%d and %d is highest number", num2, num3);
	}
	else if (num3 > num2 && num3 == num1) {
		printf("%d and %d is highest number", num3, num1);
	}
	else if (num3 > num1 && num3 == num2) {
		printf("%d and %d is highest number", num3, num2);
	}
	else {
		printf("All Number is equal");
	}

	getch();
}

void Ex0804() {
	int num1, num2, num3;

	printf("Enter Number 1: ");
	scanf("%d", &num1);
	printf("Enter Number 2: ");
	scanf("%d", &num2);
	printf("Enter Number 3: ");
	scanf("%d", &num3);

	if (num1 < num2 && num1 < num3) {
		printf("%d is lowest number", num1);
	}
	else if (num2 < num1 && num2 < num3) {
		printf("%d is lowest number", num2);
	}
	else if (num3 < num1 && num3 < num3) {
		printf("%d is lowest number", num3);
	}
	else if (num1 < num3 && num1 == num2) {
		printf("%d and %d is lowest number", num1, num2);
	}
	else if (num1 < num2 && num1 == num3) {
		printf("%d and %d is lowest number", num1, num3);
	}
	else if (num2 < num3 && num2 == num1) {
		printf("%d and %d is lowest number", num2, num1);
	}
	else if (num2 < num1 && num2 == num3) {
		printf("%d and %d is lowest number", num2, num3);
	}
	else if (num3 < num2 && num3 == num1) {
		printf("%d and %d is lowest number", num3, num1);
	}
	else if (num3 < num1 && num3 == num2) {
		printf("%d and %d is lowest number", num3, num2);
	}
	else {
		printf("All Number is equal");
	}

	getch();
}

void Ex0805() {
	int yearOfBirth;
	char name[100];
	char *statsAge = "";

	printf("Enter Name: ");
	scanf("%s", name);
	printf("Enter Year of birth (Po.Sor.): ");
	scanf("%d", &yearOfBirth);

	if (yearOfBirth >= 2561) {
		printf("You are not born yet");
	}
	else {
		yearOfBirth = (yearOfBirth - 2561) * (-1);
		if (yearOfBirth >= 55) {
			statsAge = "Old";
		}
		else if (yearOfBirth >= 20) {
			statsAge = "Adult";
		}
		else {
			statsAge = "Child";
		}
		printf("Hello, %s\n", name);
		printf("You age is %d, you is %s", yearOfBirth, statsAge);
	}

	getch();
}

void Ex0806() {
	char grade;
	char *msg = "";
	printf("Enter your grade char: ");
	scanf("%s", &grade);

	if (grade == 'A' || grade == 'a') {
		msg = "Excellent";
	}
	else if (grade == 'B' || grade == 'b') {
		msg = "Good";
	}
	else if (grade == 'C' || grade == 'c') {
		msg = "Fair";
	}
	else if (grade == 'D' || grade == 'd') {
		msg = "Pass";
	}
	else if (grade == 'F' || grade == 'f') {
		msg = "Fail";
	}
	else {
		msg = "Wrong Input";
	}

	printf("%s", msg);
	getch();
}

void Ex0807() {
	int height, weight;

	float result;
	char *strResult;

	printf("Enter height: ");
	scanf("%d", &height);
	printf("Enter weight: ");
	scanf("%d", &weight);

	result = height - 100;
	result -= weight;

	if (result < -5) {
		strResult = "Thin";
	}
	else if (result > 5) {
		strResult = "Fat";
	}
	else {
		strResult = "Slender";
	}

	printf("You are %s", strResult);
	getch();
}

void Ex0808() {
	char name[100];
	int monthSalary;

	float newSalary;

	printf("Employee's name: ");
	scanf("%s", &name);
	printf("Current month salary: ");
	scanf("%d", &monthSalary);

	if (monthSalary > 70000) {
		newSalary = monthSalary + ((monthSalary * 1) / 100);
	}
	else if (monthSalary >= 40000) {
		newSalary = monthSalary + ((monthSalary * 3) / 100);
	}
	else if (monthSalary >= 20000) {
		newSalary = monthSalary + ((monthSalary * 5) / 100);
	}
	else if (monthSalary >= 10000) {
		newSalary = monthSalary + ((monthSalary * 7) / 100);
	}
	else {
		newSalary = monthSalary + ((monthSalary * 10) / 100);
	}
	printf("In next year %s will get %.2f on month salary", name, newSalary);
	getch();
}

void Ex0809() {
	int high, low;

	char *msg = "";

	printf("Enter high pulse: ");
	scanf("%d", &high);
	printf("Enter low pulse: ");
	scanf("%d", &low);

	if ((high < 120) && (low < 80)) {
		msg = "Best";
	}
	else if ((high < 130) && (low >= 80 && low < 85)) {
		msg = "Narmal";
	}
	else if ((high < 140) && (low >= 85 && low < 90)) {
		msg = "Narmal High";
	}
	else if ((high < 150) && (low >= 90 && low < 100)) {
		msg = "Danger Level 1";
	}
	else if ((high < 180) && (low >= 100 && low < 110)) {
		msg = "Danger Level 2";
	}
	else if ((high >= 180) && (low >= 110)) {
		msg = "Danger Level 3";
	}
	else {
		msg = "Error";
	}

	printf("High: %d\nLow: %d\nStatus: %s", high, low, msg);
}

void Ex0810() {
	int x, y;
	char *position = "";
	printf("Enter Value of X: ");
	scanf("%d", &x);
	printf("Enter Value of Y: ");
	scanf("%d", &y);

	if (x > 0 && y > 0) {
		position = "Quadrant 1 (+ | +)";
	}
	else if (x < 0 && y > 0) {
		position = "Quadrant 2 (- | +)";
	}
	else if (x < 0 && y < 0) {
		position = "Quadrant 3 (- | -)";
	}
	else if (x > 0 && y < 0) {
		position = "Quadrant 4 (+ | -)";
	}
	else if (x == 0 && y != 0) {
		position = "Y-Axis (0 | Y)";
	}
	else if (x != 0 && y == 0) {
		position = "X-Axis (X | 0)";
	}
	else {
		position = "Origin (0 | 0)";
	}

	printf("%d, %d is at %s", x, y, position);
}

void Week8() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Week 8 (25/09/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\n1. Demo01.cpp");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					demo0801();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. Ex01\n2. Ex02\n3. Ex03\n4. Ex04\n5. Ex05\n6. Ex06\n7. Ex07\n8. Ex08\n9. Ex09\n10. Ex10");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					Ex0801();
					x = 0;
				}
				else if (x == 2) {
					Ex0802();
					x = 0;
				}
				else if (x == 3) {
					Ex0803();
					x = 0;
				}
				else if (x == 4) {
					Ex0804();
					x = 0;
				}
				else if (x == 5) {
					Ex0805();
					x = 0;
				}
				else if (x == 6) {
					Ex0806();
					x = 0;
				}
				else if (x == 7) {
					Ex0807();
					x = 0;
				}
				else if (x == 8) {
					Ex0808();
					x = 0;
				}
				else if (x == 9) {
					Ex0809();
					x = 0;
				}
				else if (x == 10) {
					Ex0810();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void Mid01() {
	int sharePeople, diameter;
	float areaShare, degreeCake;

	printf("Enter Diameter: ");
	scanf("%d", &diameter);
	printf("Enter Friend: ");
	scanf("%d", &sharePeople);

	areaShare = (3.141592653589793*((diameter / 2)*(diameter / 2))) / sharePeople;
	degreeCake = 360.00 / sharePeople;

	printf("Area: %.3f\nDegree: %.3f", areaShare, degreeCake);

	getch();
}

void Mid02() {
	float width, length, height, cost;
	float areaSideI, areaFloor, areaSideII, price;
	printf("Enter Width: ");
	scanf("%f", &width);
	printf("Enter Length: ");
	scanf("%f", &length);
	printf("Enter Height: ");
	scanf("%f", &height);
	printf("Enter Cost: ");
	scanf("%f", &cost);

	areaSideI = width * height;
	areaFloor = width * length;
	areaSideII = length * height;

	price = (areaFloor + areaSideI + areaSideII) * cost;
	areaFloor += areaSideI + areaSideII;

	printf("Area: %.3f\nPay: %.3f THB", areaFloor, price);
	getch();
}

void Mid03() {
	int length;
	float capacit_sph, capacit_waste, areaSquare;
	printf("Enter Length one side: ");
	scanf("%d", &length);

	areaSquare = length * length*length;
	capacit_sph = (3.141592653589793*((length / 2.00)*(length / 2.00))) * length;
	capacit_waste = areaSquare - capacit_sph;

	printf("Capacit_Sph: %.3f\nCapacit_Waste: %.3f", capacit_sph, capacit_waste);
	getch();
}

void Mid06() {
	int weekly;
	char name[10];
	float condition, monthly, yearly, bonus;

	printf("Enter Weekly Salary: ");
	scanf("%d", &weekly);
	printf("Enter Player Name: ");
	scanf("%s", &name);

	monthly = weekly * 4.00;
	yearly = monthly * 12.00;
	condition = (monthly * 15) / 100;

	if (condition > 45000) {
		bonus = (yearly * 1.25) / 100;
		printf("%s have %.2f $/year\nBonus(1.25%%) = %.2f $", name, yearly, bonus);
	}
	else {
		bonus = (yearly * 2.75) / 100;
		printf("%s have %.2f $/year\nBonus(2.75%%) = %.2f $", name, yearly, bonus);
	}
	getch();
}

void Mid08() {
	int meter1, meter2;
	char name[10];
	float cost;

	printf("Enter Meter before: ");
	scanf("%d", &meter1);
	printf("Enter Meter after: ");
	scanf("%d", &meter2);
	printf("Enter Owner Name: ");
	scanf("%s", &name);

	if (meter1 > meter2 && meter1 > 0 && meter2 > 0) {
		meter1 -= meter2;
		meter2 = meter1;
	}
	else if (meter1 < meter2 && meter1 > 0 && meter2 > 0) {
		meter2 -= meter1;
	}
	else {
		printf("Error");
		meter2 = -1;
	}

	if (meter2 > 150) {
		cost = meter2 * 27.72;
		printf("%s use %d unit\nCost: %.2f", name, meter2, cost);
	}
	else if (meter2 > 96) {
		cost = meter2 * 9.25;
		printf("%s use %d unit\nCost: %.2f", name, meter2, cost);
	}
	else if (meter2 > 0) {
		printf("%s use %d unit\nCost: Free Pay", name, meter2);
	}
	getch();
}

void Midterm() {
	int x;
	x = 0;
	while (x == 0) {
		printf("\nWelcome to Midterm (05/10/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher\nDo not have any work on this folder.");
				/*printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				if(x==0){
					break;
				} else if(x==1){
					demo0701();
					x = 0;
				} else if(x==2){
					demo0702();
					x = 0;
				} else if(x==3){
					demo0703();
					x = 0;
				} else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;*/
				break;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher\n1. Mid01\n2. Mid02\n3. Mid03\n4. Mid06\n5. Mid08");
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				if (x == 0) {
					break;
				}
				else if (x == 1) {
					Mid01();
					x = 0;
				}
				else if (x == 2) {
					Mid02();
					x = 0;
				}
				else if (x == 3) {
					Mid03();
					x = 0;
				}
				else if (x == 4) {
					Mid06();
					x = 0;
				}
				else if (x == 5) {
					Mid08();
					x = 0;
				}
				else {
					printf("Wrong Input please select one\n\n");
					x = 0;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void demo0901() {
	printf("\\\\// \"100%%\" \\\\//");
	getch();
}

void demo0902() {
	float a = 10.56;
	int b = (int)a;
	a -= b;
	printf("%.2f", a);
	getch();
}

void demo0903() {
	char x = 'A';
	while (x < 'Z' + 1) {
		printf("%c ", x);
		x++;
	}
	getch();
}

void demo0904() {
	int x = 1;
	while (x < 11) {
		if (x % 2 != 0) {
			printf("%d ", x);
		}
		x++;
	}
	getch();
}

void demo0905() {
	int x = 1;
	while (x < 11) {
		if (x % 3 == 0) {
			printf("%d ", x);
		}
		x++;
	}
	getch();
}

void demo0906() {
	int x = 1;
	int y;
	printf("Enter Number: ");
	scanf("%d", &y);
	while (x <= y) {
		printf("Hello %d\n", x);
		x++;
	}
	getch();
}

void demo0907() {
	int x = 1;
	int sum = 0, y = 0;

	printf("Enter Number: ");
	scanf("%d", &y);
	while (x <= y) {
		sum = sum + x;
		printf("%d\n", sum);
		x++;
	}
}

void Week9() {
	int x;
	x = 0;
	while (x == 0) {
	breakLoop:
		printf("\nWelcome to Week 9 (16/10/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		else if (x == 1) {
			x = 0;
			while (x == 0) {
				printf("\nDemo from Teacher");
				for (int i = 1; i <= 7; i++) {
					printf("\n%d. Demo%d", i, i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				switch (x) {
				case 0:
					goto breakLoop;
				case 1:
					demo0901();
					break;
				case 2:
					demo0902();
					break;
				case 3:
					demo0903();
					break;
				case 4:
					demo0904();
					break;
				case 5:
					demo0905();
					break;
				case 6:
					demo0906();
					break;
				case 7:
					demo0907();
					break;
				default:
					printf("Wrong Input please select one\n\n");
					break;
				}
				x = 0;
			}
		}
		else if (x == 2) {
			x = 0;
			while (x == 0) {
				printf("\nExercise from Teacher");
				for (int i = 1; i <= 0; i++) {
					printf("\n%d. Ex%d", i, i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d", &x);
				switch (x) {
				case 0:
					goto breakLoop;
				case 1:
					//Ex0901();
					break;
				default:
					printf("Wrong Input please select one\n\n");
					break;
				}
				x = 0;
			}
		}
		else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void hw0101() {
	int x;
	int y = 0;
	while (true) {
		printf("\nEnter Number for plus (-9999 will stop calculate): ");
		scanf("%d", &x);

		if (x == -9999) {
			printf("Total: %d", y);
			getch();
			break;
		}
		else {
			y += x;
			printf("%d", y);
		}
	}
}

void hw0102() {
	int x;
	int y = 0;
	printf("Enter Number for do total sum: ");
	scanf("%d", &x);
	while (x > 0) {
		y += x;
		printf("\n%d", y);
		x -= 1;
	}
	printf("\nResult is %d", y);
	getch();
}

void hw0103() {
	int x = 12;
	while (x > 0) {
		printf("\n2 x %d", x);
		x -= 1;
	}
}

void hw0104() {
	int x = 0, z=0;
	int y = 0;
	printf("Enter for base: ");
	scanf("%d", &x);
	printf("Enter for power: ");
	scanf("%d", &y);
	z = x;

	if (y == 0) {
		printf("\nResult is 1");
	}
	else {
		while (y > 0) {
			x = x*z;
			y = y - 1;
		}
		printf("\nResult is %d", x);
	}
}

void hw0105() {
	int x = 2;
	int y = 1;
	while (y <= 10) {
		printf("\n%d", x);
		x *= 2;
		y += 1;
	}
}

void hw01() {
	int x;
	x = 0;
	while (x == 0) {
	breakLoop:
		printf("\nWelcome to Homework 01 (16/10/61)\n");
		printf("\nHomework from Teacher");
		for (int i = 1; i <= 5; i++) {
			printf("\n%d. Homework%d", i, i);
		}
		printf("\nSelect files you want to view (0 as back to MainMenu): ");
		scanf("%d", &x);

		switch (x) {
		case 0:
			break;
		case 1:
			hw0101();
			break;
		case 2:
			hw0102();
			break;
		case 3:
			hw0103();
			break;
		case 4:
			hw0104();
			break;
		case 5:
			hw0105();
			break;
		default:
			printf("Wrong Input please select one\n\n");
			break;
		}

		if (x == 0) {
			break;
		}

		x = 0;
	}
}

void demo1001(){
    int x = 1;
    while(x <= 10){
        printf("\n%d",x);
        x += 1;
    }
}

void demo1002(){
    int x, y;
    printf("Enter Number: ");
    scanf("%d", &x);
    y = x;
    x = 1;
    while(x <= y){
        printf("\n%d",x);
        x += 1;
    }
}

void demo1003(){
    int x;
	int y = 0;
	printf("Enter Number for do total sum: ");
	scanf("%d", &x);
	while (x > 0) {
		y += x;
		printf("\n%d", y);
		x -= 1;
	}
	printf("\nResult is %d", y);
	getch();
}

void demo1004(){
    int x,y = 0;
    int total = 0;
    float mean = 0.0;
    printf("Enter Number: ");
    scanf("%d", &x);
    y = x;
    while(x > 0){
        total += x;
        x -= 1;
    }
    printf("Total sum is %d", total);
    mean = total/(y*1.00);
    printf("\nMean is %.2f", mean);
}

void demo1005(){
    int amtStudent, amt=0;
    int scoreMid, scoreFinal, scoreHomework, studentScore, scoreTotal = 0;
    float totalMean = 0.0;
    char idStudent[12];

    printf("Enter amount of student: ");
    scanf("%d", &amtStudent);

    while(amt < amtStudent){
        printf("Enter Id Student: ");
        scanf("%s", &idStudent);
        printf("Enter score Midterm: ");
        scanf("%d", &scoreMid);
        printf("Enter score Final: ");
        scanf("%d", &scoreFinal);
        printf("Enter score Homework: ");
        scanf("%d", &scoreHomework);

        studentScore = scoreFinal+scoreHomework+scoreMid;

        printf("%s\ is got %d score\n", idStudent, studentScore);
        scoreTotal += studentScore;

        amt += 1;
    }

    totalMean = scoreTotal/(amtStudent*1.00);
    printf("Overall student mean score is %.2f", totalMean);
}

void Ex1001(){
    int amtStudent;
    int scoreMid, scoreFinal, scoreHomework, studentScore, scoreTotal = 0;
    float totalMean = 0.0;
    char idStudent[12];

    printf("Enter amount of student: ");
    scanf("%d", &amtStudent);

    for(int i=0; i < amtStudent; i++){
        printf("Enter Id Student: ");
        scanf("%s", &idStudent);
        printf("Enter score Midterm: ");
        scanf("%d", &scoreMid);
        printf("Enter score Final: ");
        scanf("%d", &scoreFinal);
        printf("Enter score Homework: ");
        scanf("%d", &scoreHomework);

        studentScore = scoreFinal+scoreHomework+scoreMid;

        printf("%s\ is got %d score\n", idStudent, studentScore);
        scoreTotal += studentScore;
    }

    totalMean = scoreTotal/(amtStudent*1.00);
    printf("Overall student mean score is %.2f", totalMean);
}

void Ex1002(){
    int amtStudent, amt=0;
    int scoreMid, scoreFinal, scoreHomework, studentScore, scoreTotal = 0;
    float totalMean = 0.0;
    int idStudent;
    while(true){
        printf("Enter Id Student: ");
        scanf("%d", &idStudent);
        if(idStudent == -999){
            break;
        } else{
            printf("Enter score Midterm: ");
            scanf("%d", &scoreMid);
            printf("Enter score Final: ");
            scanf("%d", &scoreFinal);
            printf("Enter score Homework: ");
            scanf("%d", &scoreHomework);

            studentScore = scoreFinal+scoreHomework+scoreMid;

            printf("%d\ is got %d score\n", idStudent, studentScore);
            scoreTotal += studentScore;

            amtStudent += 1;
        }
    }

    totalMean = scoreTotal/(amtStudent*1.00);
    printf("Overall student mean score is %.2f", totalMean);
}

void week10(){
	int x;
	x = 0;
	while(x==0){
		breakLoop:
		printf("\nWelcome to Week 10 (23/10/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if(x==0){
			break;
		} else if(x==1){
			x = 0;
			while(x==0){
				printf("\nDemo from Teacher");
				for(int i = 1;i<=5;i++){
					printf("\n%d. Demo%d",i,i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				switch(x){
					case 0:
						goto breakLoop;
					case 1:
						demo1001();
						break;
                    case 2:
						demo1002();
						break;
                    case 3:
						demo1003();
						break;
                    case 4:
						demo1004();
						break;
                    case 5:
						demo1005();
						break;
					default:
						printf("Wrong Input please select one\n\n");
						break;
				}
				x = 0;
			}
		} else if(x==2){
			x = 0;
			while(x==0){
				printf("\nExercise from Teacher");
				for(int i = 1;i<=2;i++){
					printf("\n%d. Ex%d",i,i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				switch(x){
					case 0:
						goto breakLoop;
					case 1:
						Ex1001();
						break;
                    case 2:
						Ex1002();
						break;
					default:
						printf("Wrong Input please select one\n\n");
						break;
				}
				x = 0;
			}
		} else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

void week11(){
	printf("System backup Error, We lost all data from this week");
	getch();
}

void demo1201(){
    int size_num, arrayId = 0, arrayValue;
    while(true){
        printf("Enter size of array (1-10): ");
        scanf("%d",&size_num);
        if(size_num > 10 || size_num < 1){
            printf("Invalid Input | Input: %d\n", size_num);
        } else{
            break;
        }
    }

    int arrayNum[size_num];

    while(arrayId < size_num){
        while(true){
            printf("Enter number to input to array[%d] (value allow at 0-20): ",arrayId);
            scanf("%d",&arrayValue);
            if(arrayValue > 20 || arrayValue < 0){
                printf("Invalid Input | Input: %d\n", arrayValue);
            } else{
                arrayNum[arrayId] = arrayValue;
                break;
            }
        }
        arrayId += 1;
    }

    arrayId = 0;

    while(arrayId < size_num){
        int counting = 0;
        printf("%d) ", arrayId);
        while(counting < arrayNum[arrayId]){
            printf("*");
            counting += 1;
        }
        printf("\n");
        arrayId += 1;
    }
    getch();
}

void ex1201(){
    int size_numว
    int aA = 0,eE = 0, iI = 0, oO = 0, uU = 0;
    char arrayChecker[10] = {'a','A','e','E','i','I','o','O','u','U'};
    while(true){
        printf("Enter size of array (30-60): ");
        scanf("%d",&size_num);
        if(size_num > 60 || size_num < 30){
            printf("Invalid Input | Input: %d\n", size_num);
        } else{
            break;
        }
    }

    while(true){
        char arrayString[size_num];
        printf("\nEnter string (? for exit): ");
        scanf("%s", &arrayString);
        if(arrayString[0] == '?'){
            break;
        } else{
            int countingStringId = 0;
            while(countingStringId < size_num){
                int countingCheckerId = 0;
                while(countingCheckerId < 10){
                    if(arrayString[countingStringId] == arrayChecker[countingCheckerId]){
                        switch(countingCheckerId){
                            case 0:
                                aA += 1;
                                break;
                            case 1:
                                aA += 1;
                                break;
                            case 2:
                                eE += 1;
                                break;
                            case 3:
                                eE += 1;
                                break;
                            case 4:
                                iI += 1;
                                break;
                            case 5:
                                iI += 1;
                                break;
                            case 6:
                                oO += 1;
                                break;
                            case 7:
                                oO += 1;
                                break;
                            case 8:
                                uU += 1;
                                break;
                            case 9:
                                uU += 1;
                                break;
                            default:
                                break;
                        }
                    }
                    countingCheckerId += 1;
                }
                countingStringId += 1;
            }
            countingStringId = 0;
            while(countingStringId < size_num){
                printf("%c ", arrayString[countingStringId]);
                countingStringId += 1;
            }

            printf("\nAnswer\n");
            printf("aA) %d\n", aA);
            printf("eE) %d\n", eE);
            printf("iI) %d\n", iI);
            printf("oO) %d\n", oO);
            printf("uU) %d\n", uU);

            aA = 0;
            eE = 0;
            iI = 0;
            oO = 0;
            uU = 0;
        }
    }
}

void week12(){
	int x;
	x = 0;
	while(x==0){
		breakLoop:
		printf("\nWelcome to Week 12 (13/11/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if(x==0){
			break;
		} else if(x==1){
			x = 0;
			while(x==0){
				printf("\nDemo from Teacher");
				for(int i = 1;i<=1;i++){
					printf("\n%d. Demo%d",i,i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				switch(x){
					case 0:
						goto breakLoop;
					case 1:
						demo1201();
						break;
					default:
						printf("Wrong Input please select one\n\n");
						break;
				}
				x = 0;
			}
		} else if(x==2){
			x = 0;
			while(x==0){
				printf("\nExercise from Teacher");
				for(int i = 1;i<=1;i++){
					printf("\n%d. Ex%d",i,i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				switch(x){
					case 0:
						goto breakLoop;
					case 1:
						ex1201();
						break;
					default:
						printf("Wrong Input please select one\n\n");
						break;
				}
				x = 0;
			}
		} else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}

// Template for each Week
/*void week9(){
	int x;
	x = 0;
	while(x==0){
		breakLoop:
		printf("\nWelcome to Week 9 (16/10/61)\n");
		printf("\nSelect files you want to view (1 as Demo / 2 as Exercise / 0 as back to MainMenu): ");
		scanf("%d", &x);
		if(x==0){
			break;
		} else if(x==1){
			x = 0;
			while(x==0){
				printf("\nDemo from Teacher");
				for(int i = 1;i<=1;i++){
					printf("\n%d. Demo%d",i,i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				switch(x){
					case 0:
						goto breakLoop;
					case 1:
						demo0901();
						break;
					default:
						printf("Wrong Input please select one\n\n");
						break;
				}
				x = 0;
			}
		} else if(x==2){
			x = 0;
			while(x==0){
				printf("\nExercise from Teacher");
				for(int i = 1;i<=1;i++){
					printf("\n%d. Ex%d",i,i);
				}
				printf("\nPlease Select work files as number (If you can remember / Input 0 = Exit to MainMenu): ");
				scanf("%d",&x);
				switch(x){
					case 0:
						goto breakLoop;
					case 1:
						ex0901();
						break;
					default:
						printf("Wrong Input please select one\n\n");
						break;
				}
				x = 0;
			}
		} else {
			printf("Wrong Input please select one\n\n");
			x = 0;
		}
	}
}*/

int main() {
	int x;
	x = 0;
	while (x == 0) {
        breakloop:
		printf("Welcome to my workstation | My Name is Napapon Kamanee | ID: 61011212098\n");
		printf("\nList of week:\n1. Week 3 (21/08/61)\n2. Week 4 (28/08/61)\n3. Week 5 (04/09/61)\n4. Week 6 (11/09/61)\n5. Week 7 (18/09/61)\n6. Week 8 (25/09/61)\n7. Midterm (05/10/61)\n8. Week 9 (16/10/61)\n9. Homework 1 (16/10/61)");
		printf("\n10. Week 10\n11. Week 11 (Data lost)\n12. Week 12");
		printf("\nSelect the Week of work (0 for exit): ");
		scanf("%i", &x);
		switch (x) {
		case 0:
			printf("Exit");
			goto breakLoop;
		case 1:
			Week3();
			break;
		case 2:
			Week4();
			break;
		case 3:
			Week5();
			break;
		case 4:
			Week6();
			break;
		case 5:
			Week7();
			break;
		case 6:
			Week8();
			break;
		case 7:
			Midterm();
			break;
		case 8:
			Week9();
			break;
		case 9:
			hw01();
			break;
        case 10:
			week10();
			break;
        case 11:
			week11();
			break;
        case 12:
			week12();
			break;
		default:
			printf("Wrong Input please select one\n\n");
			x = 0;
			continue;
		}
		printf("\nThanks for using us!! - Heading back to MainMenu\n\n");
		x = 0;
	}
    breakLoop:
	return 0;
}
