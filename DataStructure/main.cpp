#include <iostream>

using namespace std;

void bubbleSort(int data[], int length){
    int i, j, temp;
    bool isSwapped;
    for(i = 0; i < length - 1; i++){
        for(j=0; j < length - i - 1; j++){
            if(data[j] > data[j+1]){
                temp = data[j];
                data[j] = data[j+1];
                data[j+1] = temp;
                isSwapped = true;
            }
        }
        if(!isSwapped){
            break;
        }
    }
}

void instantlySort(int data[], int length){
    int i, j, temp;

    for(i = 1; i < length ; i++){
        temp = data[i];
        j = i - 1;

        while(j >= 0 && data[j] > temp){
            data[j+1] = data[j];
            j = j-1;
        }
        data[j+1] = temp;
    }
}

void selectionSort(int data[], int length){
    int i, j, min, loc, key;
    for(i = 0; i < length -1; i++){
        min = data[i];
        loc = i;
        for(j = i +1; j < length; j++){
            if(min > data[j]){
                min = data[j];
                loc = j;
            }
        }
        key = data[i];
        data[i] = data[loc];
        data[loc] = key;
    }
}

void shellSort(int data[], int length){
    int temp, j, k;
    for(k = length/2; k > 0; k/=2){
        for(int i = k; i < length; i++){
            temp = data[i];
            j = i;
            while(j > k -1 && data[j-k] > temp){
                data[j] = data[j-k];
                j = j-k;
            }
            data[j] = temp;
        }
    }
}

void printArray(int data[], int length){
    int i;
    for(i = 0; i < length; i++){
        cout << data[i] << " ";
    }
    cout << endl;
}

int main() {
    int mainArray[] = {7, 12, 9, 5, 30, 2, 9};
    int length = sizeof(mainArray) / sizeof(mainArray[0]);
    printArray(mainArray, length);
    shellSort(mainArray, length);
    printArray(mainArray, length);

    return 0;
}